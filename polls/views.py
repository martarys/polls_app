from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404

from django.template import loader
from django.urls import reverse

from .models import Question

# Create your views here.
#
#request function:
def index(request):
    # wywołanie pytań według parametru 'publish_date' - data publikacji (5 najnowszych)
    latest_questions = Question.objects.order_by('-publish_date')[:5]
    #wersja z plikiem html:

    template = loader.get_template('polls/index.html')
    #tutaj jest funkcja pobrania treści requesta - działa to poprzez zczytywanie treści pliku html,
    # który podajemy (dlatego render)
    context = ({
        'latest_questions':latest_questions
    })

    return HttpResponse(template.render(context))

    # #wersja bez użycia html: dodajemy przecinki między wynikami latest_questions -
    # output = ", ".join(q.question_text for q in latest_questions)
    # return HttpResponse(output)

def detail(request, question_id):
    # question = Question.objects.get(pk = question_id)

    # jeśli chcemy obsłużyć błąd, jeśli nie ma nic go pobrania (get):
    question = get_object_or_404(Question, pk = question_id)
    #inny sposób na renderowanie contextu z html:
    return render(request, 'polls/detail.html', {'question': question})


def results(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    return render(request, 'polls/results.html',{'question': question})

def vote(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    try:
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
    except:
        return render(request, 'polls/detail.html', {'question':question, 'error_message': 'Please, select a choice'})
    #teraz dodajemy implementację, która zliczy nam głosy:
    else:
        selected_choice.votes += 1
        selected_choice.save()
        #teraz po wykonaniu wszystkiego chcemy włączyć result page
        return HttpResponseRedirect(reverse('polls:results', args=(question_id,)))