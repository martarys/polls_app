# Polls application

This is my first polls aplication written during Udemy course - "A Begginers Guide to Django" in march 2020.

## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Source](#source)

## General info
Purpose of this project was learning and practising Django framework.


## Technologies
* Python - version 3.8
* Django - version 3.0.4
* MySQL
* HTML/CSS

## Source
Udemy course:
https://www.udemy.com/course/introdjango/
